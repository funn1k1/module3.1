using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            //Task1
            Task1 A = new Task1();
            Console.Write("Введите два числа: ");
            int number1 = A.ParseAndValidateIntegerNumber(Console.ReadLine());
            int number2 = A.ParseAndValidateIntegerNumber(Console.ReadLine());
            Console.WriteLine(A.Multiplication(number1, number2));
            //Task2
            Task2 B = new Task2();
            Console.Write("Введите натуральное число: ");
            int number = 0;
            bool tryParseNaturalNumber=B.TryParseNaturalNumber(Console.ReadLine(),out number);
            Console.WriteLine(tryParseNaturalNumber);
            List<int> vs=B.GetEvenNumbers(8);
            for(int i = 0; i < vs.Count; i++)
            {
                Console.Write(vs[i]+"|");
            }
            Console.WriteLine();
            //Task3
            Task3 C = new Task3();
            Console.WriteLine(C.RemoveDigitFromNumber(86324, 8));

        }
    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            if (int.TryParse(source,out int n)) return n;
            else throw new ArgumentException("Input line has incorrect format");
        }

        public int Multiplication(int num1, int num2)
        {
            int res = 0;
            for(int i = 1; i <= Math.Abs(num2); i++)
            {
                res += num1;
            }
            if (num2 < 0) res = -res;
            return res;

        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while(true)
            {
                if (Int32.TryParse(input, out result) && result>=0) return true;
                else
                {
                    Console.WriteLine("Incorrect data...Please, type again");
                    input = Console.ReadLine();
                }
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> vs = new List<int>();
            for(int i =0; i < naturalNumber; i++)
            {
                if (i % 2 == 0)
                {
                    vs.Add(i);
                }
            }
            return vs;
        }
    }

    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            while (true)
            {
                if (Int32.TryParse(input, out result) && result >= 0) return true;
                else
                {
                    Console.WriteLine("Incorrect data...Please, type again");
                    input = Console.ReadLine();
                }
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
            string str = source.ToString();
            int index = str.IndexOf(digitToRemove.ToString());
            while (index != -1)
            {
                str = str.Remove(index,1);
                index = str.IndexOf(digitToRemove.ToString());
            }
            return str;
        }
    }
}